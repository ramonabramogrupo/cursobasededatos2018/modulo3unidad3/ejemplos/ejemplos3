﻿/**
  Ejemplos de programacion 3
**/

USE ejemplosprogramacion3;

/**
  Ejercicio 2
  Realizar una función que reciba como argumentos:
    -	Base de un triangulo
    -	Altura de un triangulo
  Debe devolver el cálculo del área del triángulo.

  Para probar la funcion:
  
  select areaTriangulo(23,4);
**/

DELIMITER //
CREATE OR REPLACE FUNCTION areaTriangulo 
  (base int, altura int)
RETURNS float
COMMENT ' Calcula el area de un triangulo. Recibe como argumentos la base y la altura'
BEGIN
  -- creamos las variables
  DECLARE area float;

  -- realizamos los calculos
  set area=base*altura/2;
  
  -- retornamos el resultado
  RETURN area;
END //
DELIMITER ;

-- probando la funcion
SELECT areaTriangulo(12,2);

/**
  Ejercicio 3
  Realizar una función que reciba como argumentos:
    -	Base de un triangulo
    -	Lado2 de un triangulo
    -	Lado3 de un triangulo
  Debe devolver el cálculo del perimetro del triángulo.

  Para probar la funcion:
  
  select perimetroTriangulo(23,4);
**/

DELIMITER //
CREATE OR REPLACE FUNCTION perimetroTriangulo 
  (base int, lado2 int,lado3 int)
RETURNS float
COMMENT ' Calcula el perimetro de un triangulo. 
  Recibe como argumentos la base y los otros dos lados'
BEGIN
  -- creamos las variables
  DECLARE r float;

  -- realizamos los calculos
  set r=base+lado2+lado3;
  
  -- retornamos el resultado
  RETURN r;
END //
DELIMITER ;

-- probando la funcion
SELECT perimetroTriangulo(12,2,2);

/**
  Ejercicio 4
  Realizar un procedimiento almacenado que cuando le llames 
  como argumentos:
    -	Id1: id inicial
    -	Id2: id final
  Actualice el área y el perímetro de los triángulos (utilizando las funciones realizadas) que estén comprendidos entre los id pasados.


  Para probar el procedimiento:
  
  call actualizarTriangulos(1,5);
**/

DELIMITER //
CREATE OR REPLACE PROCEDURE actualizarTriangulos
  (id1 int, id2 int)
COMMENT 'Actualiza la tabla triangulos con el area y el perimetro'
BEGIN

  UPDATE triangulos
    SET 
      area=areaTriangulo(base,altura),
      perimetro=perimetroTriangulo(base,lado2,lado3)
    WHERE id BETWEEN id1 AND id2;
     
END //
DELIMITER ;
-- probando el procedimiento
CALL actualizarTriangulos(1,5);
SELECT * FROM triangulos t;



/**
Ejercicio 5

Realizar una función que reciba como argumentos:
-	Lado de un cuadrado
Debe devolver el cálculo del área

**/

DELIMITER //
CREATE OR REPLACE FUNCTION areaCuadrado(
lado int)
RETURNS int
  COMMENT 'Funcion que calcula el area de un cuadrado de lado dado'
BEGIN
  DECLARE r int;
  SET r=POW(lado,2);
  RETURN r;
END //
DELIMITER ;

SELECT areaCuadrado(3);


/**
Ejercicio 6

Realizar una función que reciba como argumentos:
-	Lado de un cuadrado
Debe devolver el perimetro de un cuadrado
**/

DELIMITER //
CREATE OR REPLACE FUNCTION perimetroCuadrado(
lado int)
RETURNS int
  COMMENT 'Funcion que calcula el perimetro de un cuadrado de lado dado'
BEGIN
  DECLARE r int;
  SET r=lado*4;
  RETURN r;
END //
DELIMITER ;

SELECT perimetroCuadrado(3);

/**
EJERCICIO 7
  
  Realizar un procedimiento almacenado que cuando le llames como argumentos:

-	Id1: id inicial
-	Id2: id final

Actualice el área y el perímetro de los cuadrados
  (utilizando las funciones realizadas) que estén comprendidos entre los id pasados.

**/

DELIMITER //
CREATE OR REPLACE PROCEDURE actualizarCuadrado(
  id1 int, 
  id2 int)
  COMMENT 'Actualiza la tabla cuadrados con el area y el perimetro'
BEGIN

  UPDATE cuadrados  
    SET 
      area=areaCuadrado(lado),
      perimetro=perimetroCuadrado(lado)
    WHERE 
      id BETWEEN id1 AND id2;

END //
DELIMITER ;

CALL actualizarCuadrado(1,10);
SELECT * FROM cuadrados;


/**
Ejercicio 8

Realizar una función que reciba como argumentos:
-	lado1 de un rectangulo
- lado2 de un rectangulo
Debe devolver el area del rectangulo
**/

DELIMITER //
CREATE OR REPLACE FUNCTION areaRectangulo(
l1 int, l2 int)
RETURNS int
  COMMENT 'Funcion que calcula el area de un rectangulo'
BEGIN
  DECLARE r int;
  SET r=l1*l2;
  RETURN r;
END //
DELIMITER ;

SELECT areaRectangulo(2,5);

/**
Ejercicio 9

Realizar una función que reciba como argumentos:
-	lado1 de un rectangulo
- lado2 de un rectangulo
Debe devolver el perimetro de un rectangulo

**/

DELIMITER //
CREATE OR REPLACE FUNCTION perimetroRectangulo(
l1 int, l2 int)
RETURNS int
  COMMENT 'Funcion que calcula el perimetro de un rectangulo'
BEGIN
  DECLARE r int;
  SET r=2*(l1+l2);
  RETURN r;
END //
DELIMITER ;

SELECT perimetroRectangulo(10,2);

/**
EJERCICIO 10
  
  Realizar un procedimiento almacenado que cuando le llames como argumentos:

-	Id1: id inicial
-	Id2: id final

Actualice el área y el perímetro de los rectángulos 
  (utilizando las funciones realizadas) que estén comprendidos entre los id pasados.

**/

DELIMITER //
CREATE OR REPLACE PROCEDURE actualizarRectangulo(
  id1 int, 
  id2 int)
  COMMENT 'Actualiza la tabla rectangulos con el area y el perimetro'
BEGIN

  UPDATE rectangulo r 
    SET 
      r.area=areaRectangulo(r.lado1,r.lado2),
      r.perimetro=perimetroRectangulo(r.lado1,r.lado2)
    WHERE 
      r.id BETWEEN id1 AND id2;

END //
DELIMITER ;

CALL actualizarRectangulo(1,10);
SELECT * FROM rectangulo r;



/**
  Ejercicio 11
  Realizar una función que reciba como argumentos:
    -	radio
  Debe devolver el cálculo del área
 
**/

  DELIMITER //
  CREATE OR REPLACE FUNCTION areaCirculo
    (radio int)
  RETURNS float
    COMMENT 'calcula el area de un circulo de radio dado'
  BEGIN
    DECLARE r float;
    
    SET r=PI()*POW(radio,2);

    RETURN r;
  END //
  DELIMITER ;

SELECT areaCirculo(2);

/**
  Ejercicio 12
  Realizar una función que reciba como argumentos:
    -	radio
  Debe devolver el perimetro del circulo
 
**/

  DELIMITER //
  CREATE OR REPLACE FUNCTION perimetroCirculo
    (radio int)
  RETURNS float
    COMMENT 'calcula el perimetro de un circulo de radio dado'
  BEGIN
    DECLARE r float;
    
    SET r=2*PI()*radio;

    RETURN r;
  END //
  DELIMITER ;

SELECT perimetroCirculo(2);

/** 
  Ejercicio 13
**/

DELIMITER //
CREATE OR REPLACE PROCEDURE actualizarCirculos
  (id1 int, id2 int, t char(1))
BEGIN

  IF(t IS NULL) THEN 
    UPDATE circulo
    SET 
      area=areaCirculo(radio),
      perimetro=perimetroCirculo(radio)
    WHERE
      id BETWEEN id1 AND id2;
  ELSE 
    UPDATE circulo
      SET 
        area=areaCirculo(radio),
        perimetro=perimetroCirculo(radio)
      WHERE
        id BETWEEN id1 AND id2
        AND
        tipo=t;
  END IF;
END //
DELIMITER ;

CALL actualizarCirculos(1,20,NULL);
SELECT * FROM circulo c;

/**
 Ejercicio 14

 Realizar una función que reciba como argumentos 4 notas.
 Debe devolver el cálculo de la nota media.
 
 **/

  DELIMITER //
  CREATE OR REPLACE FUNCTION media
    (n1 int, n2 int, n3 int, n4 int)
  RETURNS float
    COMMENT 'Realizar una función que reciba como argumentos 4 notas. Debe devolver el cálculo de la nota media.'
  BEGIN
    DECLARE r float;
    SET r=(n1+n2+n3+n4)/4;
    RETURN r;
  END //
  DELIMITER ;

/**
 Ejercicio 15

 Realizar una función que reciba como argumentos 4 notas.
 Debe devolver el cálculo de la nota minima
 
 **/

  DELIMITER //
  CREATE OR REPLACE FUNCTION minimo
    (n1 int, n2 int, n3 int, n4 int)
  RETURNS int
    COMMENT 'Realizar una función que reciba como argumentos 4 notas. Debe devolver el cálculo de la nota minima'
  BEGIN
    DECLARE r int;
    set r=LEAST(n1,n2,n3,n4);
    RETURN r;
  END //
  DELIMITER ;
  
SELECT minimo(4,12,8,-9);

/**
 Ejercicio 16

 Realizar una función que reciba como argumentos 4 notas.
 Debe devolver el cálculo de la nota maxima
 
 **/

  DELIMITER //
  CREATE OR REPLACE FUNCTION maximo
    (n1 int, n2 int, n3 int, n4 int)
  RETURNS int
    COMMENT 'Realizar una función que reciba como argumentos 4 notas. Debe devolver el cálculo de la nota maxima'
  BEGIN
    DECLARE r int;
    set r=GREATEST(n1,n2,n3,n4);
    RETURN r;
  END //
  DELIMITER ;
  
SELECT maximo(4,12,8,-9);


/**
  Ejercicio 17
  Realizar una función que reciba como argumentos 4 notas.
  Debe devolver la nota que más se repite

 **/ 


DELIMITER //
CREATE OR REPLACE FUNCTION moda(
  n1 int,
  n2 int,
  n3 int,
  n4 int)
RETURNS int
  COMMENT ' Calcula la moda de 4 numeros pasados como argumentos'
BEGIN
  DECLARE r int;
  CREATE OR REPLACE TEMPORARY TABLE temp(
    id int AUTO_INCREMENT,
    valor int,
    PRIMARY KEY (id)
    );

  INSERT INTO temp(valor) VALUES (n1),(n2),(n3),(n4);

  SELECT valor INTO r FROM temp 
  GROUP BY valor
  ORDER BY COUNT(*) DESC LIMIT 1;
  
    
  RETURN r;
END //
DELIMITER ;

SELECT moda(1,2,2,2);

/**
  Ejercicio 17A
  Realizar una función que reciba como argumentos 4 notas.
  Debe devolver la nota que más se repite

 **/ 


DELIMITER //
CREATE OR REPLACE FUNCTION moda1(
  n1 int,
  n2 int,
  n3 int,
  n4 int)
RETURNS float
  COMMENT ' Calcula la moda de 4 numeros pasados como argumentos'
BEGIN
  DECLARE r float;
  DECLARE maximo int;
  CREATE OR REPLACE TEMPORARY TABLE temp(
    id int AUTO_INCREMENT,
    valor int,
    PRIMARY KEY (id)
    );

  INSERT INTO temp(valor) VALUES (n1),(n2),(n3),(n4);

  SELECT MAX(n) INTO maximo FROM (SELECT valor,COUNT(*) n FROM temp
    GROUP BY valor) c1;
  
    SELECT AVG(valor) INTO r FROM (SELECT valor,COUNT(*) n FROM temp 
    GROUP BY valor) c1 WHERE n=maximo;
   
  RETURN r;
END //
DELIMITER ;

SELECT moda1(1,1,2,2);

/**
  ejercicio 18

  Realizar un procedimiento almacenado que cuando le llames como argumentos:
-	Id1: id inicial
-	Id2: id final
Actualice las notas mínima, máxima, media y moda de los alumnos (utilizando las funciones realizadas) que estén comprendidos entre los id pasados.
Además, el mismo procedimiento debe actualizar la tabla grupos colocando la nota media de los alumnos que estén comprendidos entre los id pasados y por grupo.

**/


DELIMITER //
CREATE OR REPLACE PROCEDURE actualizarAlumnos(
 IN id1 int,
 IN id2 int)
COMMENT ' Realizar un procedimiento almacenado que cuando le llames como argumentos:
-	Id1: id inicial
-	Id2: id final
Actualice las notas mínima, máxima, media y moda de los alumnos (utilizando las funciones realizadas) que estén comprendidos entre los id pasados.
Además, el mismo procedimiento debe actualizar la tabla grupos colocando la nota media de los alumnos que estén comprendidos entre los id pasados y por grupo.'
BEGIN
  UPDATE alumnos 
    SET 
    media=media(nota1,nota2,nota3,nota4),
    min=minimo(nota1,nota2,nota3,nota4),
    max=maximo(nota1,nota2,nota3,nota4),
    moda=moda(nota1,nota2,nota3,nota4)
    WHERE 
      id BETWEEN id1 AND id2;

  UPDATE grupos
    SET media=(SELECT AVG(media) FROM alumnos WHERE grupo=1
    AND id BETWEEN id1 AND id2)
    WHERE id=1;
  
  UPDATE grupos
    SET media=(SELECT AVG(media) FROM alumnos WHERE grupo=2
    AND id BETWEEN id1 AND id2)
    WHERE id=2;

  /*UPDATE grupos JOIN 
    (SELECT AVG(media) mediaCalculada,grupo 
      FROM alumnos 
      WHERE id BETWEEN id1 AND id2
      GROUP BY grupo) c1 ON grupos.id=c1.grupo
    SET grupos.media=c1.mediaCalculada;*/
  

 END //
 DELIMITER ;

CALL actualizarAlumnos(1,10);

/**
  Realizar un procedimiento almacenado que le pasas como argumento 
  un id y te calcula la conversión de unidades de todos los registros
  que estén detrás de ese id.
  Si está escrito los cm calculara m, km y pulgadas. 
  Si está escrito en m calculara cm, km y pulgadas 
  y así consecutivamente.
**/
DELIMITER //
CREATE OR REPLACE PROCEDURE actualizarConversion(
  IN idInicio int)
BEGIN
UPDATE conversion 
  SET 
    m=cm/100,
    km=cm/100000,
    pulgadas=cm/0.393701
  WHERE cm IS NOT NULL AND id>idInicio;

  UPDATE conversion 
  SET 
    cm=m*100,
    km=m/1000,
    pulgadas=cm/0.393701
  WHERE m IS NOT NULL AND cm IS NULL AND id>idInicio;

  UPDATE conversion 
  SET 
    cm=km*100000,
    m=km*1000,
    pulgadas=cm/0.393701
  WHERE km IS NOT NULL AND cm IS NULL AND id>idInicio;

  UPDATE conversion 
  SET 
    cm=pulgadas*2.54,
    m=pulgadas*254,
    km=pulgadas*254000
  WHERE pulgadas IS NOT NULL AND cm IS NULL AND id>idInicio;

END //
DELIMITER ;

CALL actualizarConversion(4);