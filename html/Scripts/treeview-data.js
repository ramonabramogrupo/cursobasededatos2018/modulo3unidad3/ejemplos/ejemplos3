$(document).ready(function () {
var data = [{"text":"Cover Page","icon":"images/folder.svg","href":"startpage.html","target":"DATA"},{"text":"Servers","icon":"images/folder.svg","href":"Servers\\Servers.html","target":"DATA"},{"text":"servidor mio","icon":"images/folder.svg","href":"Servers\\servidor_mio\\servidor_mio.html","target":"DATA","nodes":[{"text":"Databases","icon":"images/folder.svg","href":"Servers\\servidor_mio\\Databases\\Databases.html","target":"DATA","nodes":[{"text":"ejemplosprogramacion3","icon":"images/database.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\ejemplosprogramacion3.html","target":"DATA","nodes":[{"text":"Tables","icon":"images/folder.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Tables\\Tables.html","target":"DATA","nodes":[{"text":"alumnos","icon":"images/table.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Tables\\alumnos.html","target":"DATA"},{"text":"circulo","icon":"images/table.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Tables\\circulo.html","target":"DATA"},{"text":"conversion","icon":"images/table.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Tables\\conversion.html","target":"DATA"},{"text":"cuadrados","icon":"images/table.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Tables\\cuadrados.html","target":"DATA"},{"text":"grupos","icon":"images/table.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Tables\\grupos.html","target":"DATA"},{"text":"rectangulo","icon":"images/table.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Tables\\rectangulo.html","target":"DATA"},{"text":"triangulos","icon":"images/table.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Tables\\triangulos.html","target":"DATA"}]},{"text":"Procedures","icon":"images/folder.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Procedures\\Procedures.html","target":"DATA","nodes":[{"text":"actualizarAlumnos","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Procedures\\actualizarAlumnos.html","target":"DATA"},{"text":"actualizarCirculos","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Procedures\\actualizarCirculos.html","target":"DATA"},{"text":"actualizarConversion","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Procedures\\actualizarConversion.html","target":"DATA"},{"text":"actualizarCuadrado","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Procedures\\actualizarCuadrado.html","target":"DATA"},{"text":"actualizarRectangulo","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Procedures\\actualizarRectangulo.html","target":"DATA"},{"text":"actualizarRectangulos","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Procedures\\actualizarRectangulos.html","target":"DATA"},{"text":"actualizarTriangulos","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Procedures\\actualizarTriangulos.html","target":"DATA"}]},{"text":"Functions","icon":"images/folder.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Functions\\Functions.html","target":"DATA","nodes":[{"text":"areaCirculo","icon":"images/function.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Functions\\areaCirculo.html","target":"DATA"},{"text":"areaCuadrado","icon":"images/function.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Functions\\areaCuadrado.html","target":"DATA"},{"text":"areaRectangulo","icon":"images/function.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Functions\\areaRectangulo.html","target":"DATA"},{"text":"areaTriangulo","icon":"images/function.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Functions\\areaTriangulo.html","target":"DATA"},{"text":"maximo","icon":"images/function.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Functions\\maximo.html","target":"DATA"},{"text":"media","icon":"images/function.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Functions\\media.html","target":"DATA"},{"text":"minimo","icon":"images/function.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Functions\\minimo.html","target":"DATA"},{"text":"moda","icon":"images/function.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Functions\\moda.html","target":"DATA"},{"text":"moda1","icon":"images/function.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Functions\\moda1.html","target":"DATA"},{"text":"perimetroCirculo","icon":"images/function.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Functions\\perimetroCirculo.html","target":"DATA"},{"text":"perimetroCuadrado","icon":"images/function.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Functions\\perimetroCuadrado.html","target":"DATA"},{"text":"perimetroRectangulo","icon":"images/function.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Functions\\perimetroRectangulo.html","target":"DATA"},{"text":"perimetroTriangulo","icon":"images/function.svg","href":"Servers\\servidor_mio\\Databases\\ejemplosprogramacion3\\Functions\\perimetroTriangulo.html","target":"DATA"}]}]}]}]}];
$('#tree').treeview({levels: 3,data: data,enableLinks: true,injectStyle: false,highlightSelected: true,collapseIcon: 'images/tree-node-expanded.svg',expandIcon: 'images/tree-node-collapsed.svg'});
});
var loadEvent = function () {

  $('#btn-expand-nodes').on('click', function (e) {
    $('#tree').treeview('expandAll', { silent: true });
  });
  $('#btn-collapse-nodes').on('click', function (e) {
    $('#tree').treeview('collapseAll', { levels:3, silent: true });
  });
  
  var searchTimeOut;
  $('#input-search').on('input', function() {
    if(searchTimeOut != null)
      clearTimeout(searchTimeOut);
    searchTimeOut = setTimeout(function(){
      var pattern = $('#input-search').val();
      var tree = $('#tree');
      tree.treeview('collapseAll', { levels:3, silent: true });
      var options = { ignoreCase: true, exactMatch: false, revealResults: true };
      var results = tree.treeview('search', [pattern, options]);
    }, 500);
  });
  
  $('#tree').on('nodeSelected', function(event, data) {
    // navigate to link
    window.open (data.href, 'DATA', false)
  });
  // select first node.
  $('#tree').treeview('selectNode', [0, { silent: false }]);
}

if (window.addEventListener) {
  window.addEventListener('load', loadEvent, false);
}
else if (window.attachEvent) {
  window.attachEvent('onload', loadEvent);
}